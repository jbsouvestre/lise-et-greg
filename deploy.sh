#!/usr/bin/env bash

./node_modules/.bin/node-sass styles/main.scss css/main.css

echo "Copying index.html"
cp index.html public
echo "Copying css"
cp -r css/ public/
echo "Copying assets"
cp -r assets/ public/

echo "Done"

echo "Deploying"
git add .
git commit -m "deploy"
git push -u origin master
echo "Done deploying"
